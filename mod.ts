import { Page, path, type Plugin, sass } from './deps.ts';

type Options = Omit<sass.StringOptions<'sync'>, 'url' | 'syntax'>;

function SassPlugin(options: Options = {}): Plugin {
	return (site) => {
		site.loadAssets(['.scss', '.sass']);
		site.process(['.scss', '.sass'], (file) => {
			if (path.basename(file.src.path).startsWith('_')) {
				return;
			}

			const output = sass.compileString(file.content as string, {
				...options,
				url: path.toFileUrl(site.src(file.src.path + file.src.ext)),
				syntax: file.src.ext === '.sass' ? 'indented' : 'scss',
			});

			file.updateDest({
				ext: '.css',
			});

			file.content = output.css;

			if (output.sourceMap) {
				file.content += `\n/*# sourceMappingURL=${
					path.basename(file.dest.path + file.dest.ext + '.map')
				} */`;

				// Add a `file` property to the sourcemap
				output.sourceMap.file = path.basename(file.dest.path + file.dest.ext);

				// sass source-maps use file URLs (eg. "file:///foo/bar"), but
				// relative paths (eg. "../bar") look better in the dev-tools.
				// Also, the sass CLI tool produces relative paths.
				output.sourceMap.sources = output.sourceMap.sources.map(
					(fileUrl: string) => (
						path.relative(
							site.dest(path.dirname(file.dest.path + file.dest.ext)),
							path.fromFileUrl(new URL(fileUrl)),
						)
					),
				);

				const mapFile = Page.create(
					file.dest.path + file.dest.ext + '.map',
					JSON.stringify({
						...output.sourceMap,
						file: path.basename(file.dest.path + file.dest.ext),
					}),
				);

				site.pages.push(mapFile);
			}
		});
	};
}

export default SassPlugin;
