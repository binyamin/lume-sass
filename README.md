# Sass for Lume

A [sass](https://sass-lang.com/) plugin for [Lume](https://lume.land), a
static-site generator for [Deno](https://deno.land).

## Usage

Import the module from GitLab, using https://denopkg.dev:

```js
import sass from 'https://denopkg.dev/gl/binyamin/lume-sass@v0.2.0/mod.ts';
```

The options are pretty much identical to those which the Dart-Sass NPM package
exposes for `compileString`
([docs](https://sass-lang.com/documentation/js-api)).

## Legal

All source-code is provided under the terms of
[the MIT license](https://gitlab.com/binyamin/lume-sass/-/blob/main/LICENSE).
Copyright 2022 Binyamin Aron Green.
