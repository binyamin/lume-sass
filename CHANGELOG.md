# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project tries to adhere to
[Semantic Versioning (SemVer)](https://semver.org/spec/v2.0.0.html).

<!--
	**Added** for new features.
	**Changed** for changes in existing functionality.
	**Deprecated** for soon-to-be removed features.
	**Removed** for now removed features.
	**Fixed** for any bug fixes.
	**Security** in case of vulnerabilities.
-->

## [0.2.0] - 2022-07-22

### Changed

- Use [a different deno-sass module](https://gitlab.com/binyamin/deno-sass),
  which works better with Deno.

## [0.1.0] - 2022-07-01

### Added

- Compile sass files (`.scss` and `.sass`) to CSS
- Support writing source-maps

[0.1.0]: https://gitlab.com/binyamin/lume-sass/-/commits/v0.1.0
[0.2.0]: https://gitlab.com/binyamin/lume-sass/-/commits/v0.2.0
